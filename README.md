cachzo
======

cachzo sınıfı kullanımı

require("CacheZo.php");//sınıfı çağırıyoruz<br>
$cache = new CacheZo();//Sınıftan bir nesne oluşturuyoruz<br>
$cache->times = 10;//Cach süresi<br>
$cache->q = "div&id=header||div&id=content||div&id=footer";//parametreler<br>
$q = $cache->start();//cache işlemini başlatıyoruz<br>
print_r($q);//sonucu ekrana basalım


//Parametreler<br>
$cache->q = "div&id=header||div&id=content||div&id=footer";//örnek kullanımı<br>
element adı & id yada class = id adı yada class adı // birden fazla element için || kullanın 

//Bu sayfanız çalıştığında ana dizinde cache adında klasr oluşur<br>
//cache klasörüne girin<br>
//3 adet dosya oluşturulmuş olması gerekiyor. header.ch,center.ch,footer.ch<br>
// Bu dosyalar cache işlemi uyguladığınız elementlerin id yada class adına göre isimlendirilir. Süresi dolduğunda silinir ve tekrar cache işlemi uygulanır
