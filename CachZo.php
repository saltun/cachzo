<?php


class CacheZo{
	
	public $times     = null;
	public $pageHtml  = null;
	public $cacheUrl  = null;
	public $basePath  = null;
	public $cachePath = null; 
	public $cacheFolderName = null; 
	public $out = array(); 

	
	function CacheZo(){
		require_once("simple_html_dom.php");
		$this->cacheFolderName  = "cache"; 
		$this->basePath  = dirname(__DIR__);
		$this->cachePath = $this->basePath."/".$this->cacheFolderName."/";
		$this->cachePath = str_replace("\\", "/", $this->cachePath); 
		$this->cacheUrl  = "http://omerbektas.com.tr"; // or "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
	}
	
	public function start(){
		$this->pageHtml = file_get_html($this->cacheUrl);
		$this->settings();
		
		if($this->q != null){
			$this->q = $this->qGet();
		}
		return $this->out;
	}
	
	private function settings(){
		if(is_dir($this->cachePath) === false){
			mkdir($this->basePath."/".$this->cacheFolderName, 0700);
		}
	}
	 
	private function qGet(){
		$is_array = strstr($this->q,"||");
			if($is_array !== false){
				$qs = explode("||",$this->q);
					foreach($qs as $q){
						$element = explode("&",$q)[0];
						$type    = explode("&",explode("=",$q)[0])[1];
						$ty_name = explode("=",$q)[1];
						$cache_file_name = $this->cachePath.$ty_name.".ch";
						
						if(is_file($cache_file_name) !== false){
							if(time() - $this->times < filemtime($cache_file_name)){ 
								array_push($this->out,file_get_contents($cache_file_name));
							}else{
								$this->qDelete($cache_file_name);
								$this->qSet($cache_file_name,$element,$type,$ty_name);
								array_push($this->out,file_get_contents($cache_file_name));
							}
						}else{
							$this->qSet($cache_file_name,$element,$type,$ty_name);
							array_push($this->out,file_get_contents($cache_file_name));
						}
					}
			}else{
				$element = explode("&",$this->q)[0];
				$type    = explode("&",explode("=",$this->q)[0])[1];
				$ty_name = explode("=",$this->q)[1];
				$cache_file_name = $this->cachePath.$ty_name.".ch";
					
					if(is_file($cache_file_name) !== false){
							if(time() - $this->times < filemtime($cache_file_name)){ echo "cachdevam<br>";
								array_push($this->out,file_get_contents($cache_file_name));
							}else{
								$this->qDelete($cache_file_name);
								$this->qSet($cache_file_name,$element,$type,$ty_name);
								array_push($this->out,file_get_contents($cache_file_name));
							}
					}else{
							$this->qSet($cache_file_name,$element,$type,$ty_name);
							array_push($this->out,file_get_contents($cache_file_name));
					}
			}
	}
	
	private function qSet($cache_file_name,$element,$type,$ty_name){							
		foreach($this->pageHtml->find($element) as $el) {
			if($el->$type == $ty_name){ 
				$ac = fopen($cache_file_name,"w+");
				fwrite($ac,$el);
				fclose($ac);
				break;
			}
		}
	}
	
	private function qDelete($cache_file_name){
		unlink($cache_file_name);
	}
	

}



?>
